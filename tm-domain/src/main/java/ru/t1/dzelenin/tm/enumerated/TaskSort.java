package ru.t1.dzelenin.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.comporator.CreatedComparator;
import ru.t1.dzelenin.tm.comporator.NameComparator;
import ru.t1.dzelenin.tm.comporator.StatusComparator;
import ru.t1.dzelenin.tm.dto.model.TaskDTO;

import java.util.Comparator;

@Getter
public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Without sort", null);

    @Nullable
    private final String name;

    @Nullable
    private final Comparator<TaskDTO> comparator;

    @Nullable
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return BY_DEFAULT;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return BY_DEFAULT;
    }

    TaskSort(@NotNull final String displayName, @Nullable final Comparator<TaskDTO> comparator) {
        this.name = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return name;
    }

    public Comparator<TaskDTO> getComparator() {
        return comparator;
    }

}
