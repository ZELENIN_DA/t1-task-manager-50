package ru.t1.dzelenin.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status created);

}
