package ru.t1.dzelenin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.model.IProjectRepository;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.model.IProjectService;
import ru.t1.dzelenin.tm.dto.model.Project;
import ru.t1.dzelenin.tm.exception.field.NameEmptyException;
import ru.t1.dzelenin.tm.exception.field.UserIdEmptyException;
import ru.t1.dzelenin.tm.repository.model.ProjectRepository;
import ru.t1.dzelenin.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.Date;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository>
        implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setUser(getUserRepository(getEntityManager()).findOneById(userId));
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(getUserRepository(getEntityManager()).findOneById(userId));
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(getUserRepository(getEntityManager()).findOneById(userId));
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

}
