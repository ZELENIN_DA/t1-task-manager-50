package ru.t1.dzelenin.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.dzelenin.tm.dto.model.AbstractUserOwnedModel;
import ru.t1.dzelenin.tm.enumerated.Status;

import java.util.Date;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @Nullable
    M create(@Nullable String userId, @Nullable String name);

    @Nullable
    M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @Nullable
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}